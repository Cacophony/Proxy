package main

import (
	"regexp"
	"strings"

	"github.com/bwmarrin/discordgo"
)

const paramPlaceholder = `([a-zA-Z0-9\-_%@]+)`

var (
	channelMessage = regexp.MustCompile(
		`^\/api\/v[0-9]+\/channels\/` + paramPlaceholder + `\/messages\/` + paramPlaceholder + `$`,
	)
	channelMessagePin = regexp.MustCompile(
		`^\/api\/v[0-9]+\/channels\/` + paramPlaceholder + `\/pins\/` + paramPlaceholder + `$`,
	)
	channelPermission = regexp.MustCompile(
		`^\/api\/v[0-9]+\/channels\/` + paramPlaceholder + `\/permissions\/` + paramPlaceholder + `$`,
	)

	guildBan = regexp.MustCompile(
		`^\/api\/v[0-9]+\/guilds\/` + paramPlaceholder + `\/bans\/` + paramPlaceholder + `$`,
	)
	guildMember = regexp.MustCompile(
		`^\/api\/v[0-9]+\/guilds\/` + paramPlaceholder + `\/members\/` + paramPlaceholder + `$`,
	)
	guildMemberRole = regexp.MustCompile(
		`^\/api\/v[0-9]+\/guilds\/` + paramPlaceholder + `\/members\/` + paramPlaceholder + `\/roles\/` + paramPlaceholder + `$`,
	)
	guildRole = regexp.MustCompile(
		`^\/api\/v[0-9]+\/guilds\/` + paramPlaceholder + `\/roles\/` + paramPlaceholder + `$`,
	)
	guildIntegration = regexp.MustCompile(
		`^\/api\/v[0-9]+\/guilds\/` + paramPlaceholder + `\/integrations\/` + paramPlaceholder + `$`,
	)

	messageReaction = regexp.MustCompile(
		`^\/api\/v[0-9]+\/channels\/` + paramPlaceholder + `\/messages\/` + paramPlaceholder + `\/reactions\/` + paramPlaceholder + `\/` + paramPlaceholder + `$`,
	)

	invite = regexp.MustCompile(
		`^\/api\/v[0-9]+\/invite\/` + paramPlaceholder + `\/$`,
	)

	webhookToken = regexp.MustCompile(
		`^\/api\/v[0-9]+\/webhooks\/` + paramPlaceholder + `\/` + paramPlaceholder + `$`,
	)

	userChannels = regexp.MustCompile(
		`^\/api\/v[0-9]+\/users\/` + paramPlaceholder + `\/channels$`,
	)
	userGuilds = regexp.MustCompile(
		`^\/api\/v[0-9]+\/users\/` + paramPlaceholder + `\/guilds$`,
	)
	userGuild = regexp.MustCompile(
		`^\/api\/v[0-9]+\/users\/` + paramPlaceholder + `\/guilds\/` + paramPlaceholder + `$`,
	)
)

func getBucketID(uri string) string {
	// not implemented:
	// EndpointUserAvatar
	// EndpointGuildIcon
	// EndpointGuildSplash
	// EndpointChannelMessageAck
	// EndpointUserNotes
	// and all endpoints without a different bucket ID

	if channelMessage.MatchString(uri) {
		parts := channelMessage.FindStringSubmatch(uri)
		return discordgo.EndpointChannelMessage(parts[1], "")
	}
	if channelMessagePin.MatchString(uri) {
		parts := channelMessagePin.FindStringSubmatch(uri)
		return discordgo.EndpointChannelMessagePin(parts[1], "")
	}
	if channelPermission.MatchString(uri) {
		parts := channelPermission.FindStringSubmatch(uri)
		return discordgo.EndpointChannelPermission(parts[1], "")
	}

	if guildBan.MatchString(uri) {
		parts := guildBan.FindStringSubmatch(uri)
		return discordgo.EndpointGuildBan(parts[1], "")
	}
	if guildMember.MatchString(uri) {
		parts := guildMember.FindStringSubmatch(uri)
		return discordgo.EndpointGuildMember(parts[1], "")
	}
	if guildMemberRole.MatchString(uri) {
		parts := guildMemberRole.FindStringSubmatch(uri)
		return discordgo.EndpointGuildMemberRole(parts[1], "", "")
	}
	if guildRole.MatchString(uri) {
		parts := guildRole.FindStringSubmatch(uri)
		return discordgo.EndpointGuildRole(parts[1], "")
	}
	if guildIntegration.MatchString(uri) {
		parts := guildIntegration.FindStringSubmatch(uri)
		return discordgo.EndpointGuildIntegration(parts[1], "")
	}

	if messageReaction.MatchString(uri) {
		parts := messageReaction.FindStringSubmatch(uri)
		return discordgo.EndpointMessageReaction(parts[1], "", "", "")
	}

	if invite.MatchString(uri) {
		return discordgo.EndpointInvite("")
	}

	if webhookToken.MatchString(uri) {
		return discordgo.EndpointWebhookToken("", "")
	}

	if userChannels.MatchString(uri) {
		return discordgo.EndpointUserChannels("")
	}
	if userGuilds.MatchString(uri) {
		return discordgo.EndpointUserGuilds("")
	}
	if userGuild.MatchString(uri) {
		parts := userGuild.FindStringSubmatch(uri)
		return discordgo.EndpointUserGuild("", parts[2])
	}

	return strings.SplitN(uri, "?", 2)[0]
}
