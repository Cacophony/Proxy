package main

import (
	"gitlab.com/Cacophony/go-kit/errortracking"
	"gitlab.com/Cacophony/go-kit/logging"
)

type config struct {
	Port                  int                  `envconfig:"PORT" default:"8000"`
	Hash                  string               `envconfig:"HASH"`
	Environment           logging.Environment  `envconfig:"ENVIRONMENT" default:"development"`
	ClusterEnvironment    string               `envconfig:"CLUSTER_ENVIRONMENT" default:"development"`
	LoggingDiscordWebhook string               `envconfig:"LOGGING_DISCORD_WEBHOOK"`
	ErrorTracking         errortracking.Config `envconfig:"ERRORTRACKING"`
}
