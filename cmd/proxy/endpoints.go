package main

import (
	"io/ioutil"
	"net/http"

	"github.com/bwmarrin/discordgo"
	raven "github.com/getsentry/raven-go"
	"github.com/go-chi/chi"
	"github.com/pkg/errors"
	"go.uber.org/zap"
)

func registerEndpoints(
	logger *zap.Logger,
	ratelimiter *discordgo.RateLimiter,
	httpClient *http.Client,
	router *chi.Mux,
) {
	router.NotFound(func(resp http.ResponseWriter, req *http.Request) {
		proxyReq, err := createProxyRequest(req)
		if err != nil {
			raven.CaptureError(err, nil)
			http.Error(
				resp,
				err.Error(),
				http.StatusInternalServerError,
			)
			return
		}
		defer proxyReq.Body.Close()

		bucketID := getBucketID(req.RequestURI)

		logger := logger.With(
			zap.String("method", req.Method),
			zap.String("request_uri", req.RequestURI),
			zap.String("bucket_id", bucketID),
		)

		proxyResp, err := doProxyRequest(
			logger,
			ratelimiter,
			httpClient,
			proxyReq,
			bucketID,
		)
		if err != nil {
			raven.CaptureError(err, nil)
			http.Error(
				resp,
				err.Error(),
				http.StatusBadGateway,
			)
			return
		}
		defer proxyResp.Body.Close()

		respData, err := ioutil.ReadAll(proxyResp.Body)
		if err != nil {
			raven.CaptureError(err, nil)
			http.Error(
				resp,
				errors.Wrap(err, "error reading proxy response").Error(),
				http.StatusBadGateway,
			)
			return
		}

		for respHeaderKey, respHeaderValues := range proxyResp.Header {
			for _, respHeaderValue := range respHeaderValues {
				resp.Header().Set(respHeaderKey, respHeaderValue)
			}
		}

		logger.Info("forwarded request",
			zap.Int("status_code", proxyResp.StatusCode),
		)

		resp.WriteHeader(proxyResp.StatusCode)

		resp.Write(respData) // nolint: errcheck
	})
}
