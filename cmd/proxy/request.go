package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"go.uber.org/zap"
)

func createProxyRequest(
	req *http.Request,
) (
	*http.Request,
	error,
) {
	proxyReq, err := http.NewRequest(
		req.Method,
		base+req.RequestURI,
		req.Body,
	)
	if err != nil {
		return req, errors.Wrap(err, "error creating proxy request")
	}

	proxyReq.Header = req.Header

	return proxyReq.WithContext(req.Context()), nil
}

func doProxyRequest(
	logger *zap.Logger,
	ratelimiter *discordgo.RateLimiter,
	httpClient *http.Client,
	req *http.Request,
	bucketID string,
) (
	*http.Response,
	error,
) {
	bucket := ratelimiter.LockBucket(bucketID)
	// TODO: check if acquiring took to long, cancel request

	proxyResp, err := httpClient.Do(req)
	if err != nil {
		bucket.Release(nil) // nolint: errcheck
		return nil, errors.Wrap(err, "error making proxy request")
	}

	proxyResp.Header = cleanHeaders(proxyResp.Header)

	err = bucket.Release(proxyResp.Header)
	if err != nil {
		return nil, errors.Wrap(err, "error releasing bucket")
	}

	// TODO: retry on Bad Gateway? like in discordgo

	if proxyResp.StatusCode == http.StatusTooManyRequests {
		respData, err := ioutil.ReadAll(proxyResp.Body)
		if err != nil {
			return nil, errors.Wrap(err, "error reading ratelimit response")
		}

		rl := discordgo.TooManyRequests{}
		err = json.Unmarshal(respData, &rl)
		if err != nil {
			return nil, errors.Wrap(err, "error unmarshalling ratelimit response")
		}

		logger.Warn("retrying request, because of ratelimit")

		time.Sleep(rl.RetryAfter * time.Millisecond)

		return doProxyRequest(logger, ratelimiter, httpClient, req, bucketID)
	}

	return proxyResp, nil
}

func cleanHeaders(get http.Header) http.Header {
	const header = "X-RateLimit-Remaining"

	if val := get.Get(header); val != "" {
		if strings.Contains(val, ".") {
			get.Set(header, strings.SplitN(val, ".", 2)[0])
		}
	}

	return get
}
