package main

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_cleanHeaders(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		give http.Header
		want http.Header
	}{
		{
			name: "do nothing if there are no headers",
			give: nil,
			want: nil,
		},
		{
			name: "do nothing if there are unrelated headers",
			give: func() http.Header {
				headers := http.Header{}
				headers.Add("X-RateLimit-Reset", "1")
				headers.Add("X-RateLimit-Global", "2")
				headers.Add("X-RateLimit-Reset-After", "3")

				return headers
			}(),
			want: http.Header{
				"X-Ratelimit-Reset":       []string{"1"},
				"X-Ratelimit-Global":      []string{"2"},
				"X-Ratelimit-Reset-After": []string{"3"},
			},
		},
		{
			name: "do nothing if the \"remaining\" header is not float",
			give: func() http.Header {
				headers := http.Header{}
				headers.Add("X-RateLimit-Remaining", "1657484779")

				return headers
			}(),
			want: http.Header{
				"X-Ratelimit-Remaining": []string{"1657484779"},
			},
		},
		{
			name: "clean \"remaining\" header",
			give: func() http.Header {
				headers := http.Header{}
				headers.Add("X-RateLimit-Remaining", "1657484779.389")

				return headers
			}(),
			want: http.Header{
				"X-Ratelimit-Remaining": []string{"1657484779"},
			},
		},
		{
			name: "clean \"remaining\" header among other headers",
			give: func() http.Header {
				headers := http.Header{}
				headers.Add("X-RateLimit-Remaining", "1657484779.389")
				headers.Add("X-RateLimit-Reset", "1")
				headers.Add("X-RateLimit-Global", "2")
				headers.Add("X-RateLimit-Reset-After", "3")

				return headers
			}(),
			want: http.Header{
				"X-Ratelimit-Remaining":   []string{"1657484779"},
				"X-Ratelimit-Reset":       []string{"1"},
				"X-Ratelimit-Global":      []string{"2"},
				"X-Ratelimit-Reset-After": []string{"3"},
			},
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			got := cleanHeaders(tt.give)
			assert.Equal(t, tt.want, got)
		})
	}
}
