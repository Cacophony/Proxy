module gitlab.com/Cacophony/Proxy

go 1.18

require (
	github.com/bwmarrin/discordgo v0.25.0
	github.com/getsentry/raven-go v0.2.0
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/kelseyhightower/envconfig v1.3.0
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.6.1
	gitlab.com/Cacophony/go-kit v0.0.0-20220709223358-ab145b942eea
	go.uber.org/zap v1.10.0
)

require (
	github.com/certifi/gocertifi v0.0.0-20190506164543-d2eda7129713 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/kr/pretty v0.3.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	go.uber.org/atomic v1.4.0 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	golang.org/x/crypto v0.0.0-20220622213112-05595931fe9d // indirect
	golang.org/x/sys v0.0.0-20220708085239-5a0f0661e09d // indirect
	golang.org/x/text v0.3.6 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)
